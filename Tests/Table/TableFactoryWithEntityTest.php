<?php

namespace  Silvioq\ReportBundle\Tests\Table;

use PHPUnit\Framework\TestCase;
use Silvioq\ReportBundle\Table\TableFactory;
use Silvioq\ReportBundle\Table\DefinitionLoader\DoctrineDefinitionLoader;
use Silvioq\ReportBundle\Table\Table;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Silvioq\ReportBundle\Tests\MockBuilder\ClassMetadataMockBuilder;
use Doctrine\DBAL\Types\Types as ORMType;

class TableFactoryWithDoctrineTest extends TestCase
{

    public function setUp() : void
    {
        // Force autoload for Annotation reader
        new \Doctrine\ORM\Mapping\Column();
    }

    public function testEntity()
    {
        /** @var EntityManagerInterface|\PHPUnit\Framework\MockObject\MockObject */
        $emMock = $this
            ->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entityMock = $this
            ->getMockBuilder(stdClass::class)
            ->getMock();

        $factory = new TableFactory();
        $factory->addLoader(new DoctrineDefinitionLoader($emMock, new AnnotationReader()), 0);

        $metadata = new ClassMetadataMockBuilder($this, $emMock, get_class($entityMock) );
        $metadata
            ->addField( 'field1', ORMType::INTEGER )
            ->addField( 'field2' )
            ->build(false);

        $table = $factory->build(get_class($entityMock));
        $this->assertInstanceOf(Table::class, $table);
    }

    /**
     * @depends testEntity
     */
    public function testEntityWithOptions()
    {
        /** @var EntityManagerInterface */
        $emMock = $this
            ->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entityMock = $this
            ->getMockBuilder(stdClass::class)
            ->getMock();

        $factory = new TableFactory();
        $factory->addLoader(new DoctrineDefinitionLoader($emMock, new AnnotationReader()), 0);

        $metadata = new ClassMetadataMockBuilder($this, $emMock, get_class($entityMock) );
        $metadata
            ->addField( 'field1', ORMType::INTEGER )
            ->addField( 'field2' )
            ->build(false);

        $table = $factory->build(get_class($entityMock), ['array_separator' => '-' ]);
        $this->assertInstanceOf(Table::class, $table);
    }

    /**
     * @depends testEntity
     */
    public function testReaderAnnotation()
    {
        /** @var EntityManagerInterface */
        $emMock = $this
            ->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $factory = new TableFactory();
        $factory->addLoader(new DoctrineDefinitionLoader($emMock, new AnnotationReader()), 0);

        $table = $factory->build(Entity\Entity::class);
        $this->assertInstanceOf( Table::class, $table );

        $this->assertEquals( [ "Age", "This name" ], $table->getHeader() );

        $entity = new Entity\Entity();
        $entity->setName('Maradona');
        $this->assertEquals( [ 42, 'Maradona'], $table->getRow($entity) );
    }

    /**
     * @depends testEntity
     */
    public function testReaderAttribute() : void
    {
        /** @var EntityManagerInterface */
        $emMock = $this
            ->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $factory = new TableFactory();
        $factory->addLoader(new DoctrineDefinitionLoader($emMock, new AnnotationReader()), 0);

        $table = $factory->build(Entity\EntityAttribute::class);
        $this->assertInstanceOf( Table::class, $table );

        $this->assertEquals( [ "Age", "This name" ], $table->getHeader() );

        $entity = new Entity\EntityAttribute();
        $entity->setName('Maradona');
        $this->assertEquals([ 42, 'Maradona'], $table->getRow($entity) );
    }

    /**
     * @depends testEntity
     */
    public function testReaderWithInvalidAnnotation()
    {
        $this->expectException(\Doctrine\Common\Annotations\AnnotationException::class);
        $this->expectExceptionMessage('The option "key" does not exist. Defined options are: "expandFinder", "expandMTM", "getter", "label", "name", "order".');

        /** @var EntityManagerInterface|\PHPUnit\Framework\MockObject\MockObject */
        $emMock = $this
            ->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $factory = new TableFactory();
        $factory->addLoader(new DoctrineDefinitionLoader($emMock, new AnnotationReader()), 0);

        $table = $factory->build(Entity\EntityWithInvalidAnnotation::class);
        $this->assertInstanceOf( Table::class, $table );
    }

    /**
     * @depends testEntity
     */
    public function testReaderWithInvalidAttribute()
    {
        $this->expectException(\Error::class);
        $this->expectExceptionMessage('Unknown named parameter $key');

        /** @var EntityManagerInterface|\PHPUnit\Framework\MockObject\MockObject */
        $emMock = $this
            ->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $factory = new TableFactory();
        $factory->addLoader(new DoctrineDefinitionLoader($emMock, new AnnotationReader()), 0);

        $table = $factory->build(Entity\EntityWithInvalidAttribute::class);
        $this->assertInstanceOf(Table::class, $table);
    }

}
