<?php

namespace  Silvioq\ReportBundle\Tests\Table;

use PHPUnit\Framework\TestCase;
use Silvioq\ReportBundle\Table\Column;

class ColumnTest extends TestCase
{

    public function testValidColumn()
    {
        $col = new Column("name");
        $this->assertEquals("name", $col->getName() );
        $this->assertEquals("Name", $col->getLabel() );
        $this->assertNull($col->getGetter());

        $col = new Column("lastName", null, "getLastName");
        $this->assertEquals("lastName", $col->getName() );
        $this->assertEquals("Last name", $col->getLabel() );
        $this->assertSame("getLastName", $col->getGetter());

        $col = new Column("lastName", "last name", function(){ return 1; } );
        $this->assertIsCallable($col->getGetter());
    }
}
