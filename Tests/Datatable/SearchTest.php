<?php

namespace  Silvioq\ReportBundle\Tests\Datatable;

use PHPUnit\Framework\TestCase;
use Silvioq\ReportBundle\Datatable\Builder;
use Silvioq\ReportBundle\Datatable\WhereBuilder;
use Doctrine\ORM\Query\Expr;
use Doctrine\DBAL\Types\Types as ORMType;

use Doctrine\DBAL\Platforms\MySQL80Platform;
use Doctrine\DBAL\Platforms\AbstractMySQLPlatform;
use Doctrine\DBAL\Platforms\MariaDb1027Platform;
use Doctrine\DBAL\Platforms\MariaDB1052Platform;
use Doctrine\DBAL\Platforms\MariaDB1060Platform;
use Doctrine\DBAL\Platforms\MariaDBPlatform;
use Doctrine\DBAL\Platforms\MySQLPlatform;
use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\DBAL\Platforms\OraclePlatform;

class SearchTest extends TestCase
{
    public function testSearchOnStringColumns(): void
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock,\Doctrine\DBAL\Platforms\AbstractMySQLPlatform::class))
            ->configure();


        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

        $expected = [
            'field1' => ORMType::STRING,
            'field2' => ORMType::STRING,
            'field3' => ORMType::JSON,
        ];

        $metadataMock->expects($this->exactly(3))
            ->method('getTypeOfField')
            ->willReturnCallback(function($field) use (&$expected) {
                $this->assertArrayHasKey($field, $expected);
                $e = $expected[$field];
                // $field debe ser el primer elemento del array
                array_shift($expected);
                return $e;
            });

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1','field2','field3']))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2, a.field3'))
            ->will($this->returnSelf() )
            ;

        $e = new Expr();
        $comp1 = $e->like('LOWER(a.field1)', ':ppp1');
        $comp2 = $e->like('LOWER(a.field2)', ':ppp1');
        $comp3 = $e->like('LOWER(a.field3)', ':ppp1');
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo(new Expr\Orx([$comp1, $comp2, $comp3] )))
            ->will($this->returnSelf() )
            ;
            
        $qbMock->expects($this->exactly(3))
            ->method('setParameter')
            ->with($this->equalTo('ppp1'),$this->equalTo('%x%'),$this->anything())
            ->will($this->returnSelf())
            ;
        
        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;
            
        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;
            
        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ 
              "search" => [ "value" => "x" ],
              "columns" => [
                   [ 'searchable' => true ],
                   [ 'searchable' => true ],
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->add( 'field3' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
        
    }

    /**
     * @dataProvider getORMTypes
     */
    public  function  testSearchStringOnNumericColumns($numericType, $dataValue = 3)
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();
        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

        $expected = [
            'field1' => $numericType,
            'field2' => ORMType::STRING,
        ];

        $metadataMock->expects($this->exactly(2))
            ->method('getTypeOfField')
            ->willReturnCallback(function($field) use (&$expected) {
                $this->assertArrayHasKey($field, $expected);
                $e = $expected[$field];
                // $field debe ser el primer elemento del array
                array_shift($expected);
                return $e;
            });

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1','field2']))
            ;


        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2'))
            ->will($this->returnSelf() )
            ;

        $e = new Expr();
        $comp2 = $e->like('LOWER(a.field2)', ':ppp1');
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo(new Expr\Orx([$comp2] )))
            ->will($this->returnSelf() )
            ;

        $qbMock->expects($this->exactly(1))
            ->method('setParameter')
            ->with($this->equalTo('ppp1'),$this->equalTo('%hello%'),$this->anything())
            ->will($this->returnSelf())
            ;

        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;
            
        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;
            
        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ 
              "search" => [ "value" => "hello" ],
              "columns" => [
                   [ 'searchable' => true ],
                   [ 'searchable' => true ],
                ],
            ] );
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }


    /**
     * @dataProvider getORMTypes
     */
    public  function  testSearchOnIntegerColumns($numericType, $dataValue = 3, $dataExpected = null)
    {
        if (null === $dataExpected)
            $dataExpected = $dataValue;

        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();
        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

    
        $expected = [
            'field1' => $numericType,
            'field2' => $numericType,
        ];

        $metadataMock->expects($this->exactly(2))
            ->method('getTypeOfField')
            ->willReturnCallback(function($field) use (&$expected) {
                $this->assertArrayHasKey($field, $expected);
                $e = $expected[$field];
                // $field debe ser el primer elemento del array
                array_shift($expected);
                return $e;
            });

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1','field2']))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2'))
            ->will($this->returnSelf() )
            ;

        $e = new Expr();
        $comp1 = $e->eq('a.field1', $dataExpected);
        $comp2 = $e->eq('a.field2', $dataExpected);
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo(new Expr\Orx([$comp1, $comp2] )))
            ->will($this->returnSelf() )
            ;
            
        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;
            
        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ 
              "search" => [ "value" => (string)$dataValue ],
              "columns" => [
                   [ 'searchable' => true ],
                   [ 'searchable' => true ],
                ],
            ] );
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }

    public function getORMTypes():array
    {
        return [
            [ ORMType::INTEGER, 3 ],
            [ ORMType::INTEGER, '3.', 3 ],
            [ ORMType::DECIMAL, 3.16 ],
            [ ORMType::SMALLINT, -4 ],
            [ ORMType::BIGINT, 2 ** 33 ],
            [ ORMType::FLOAT, 3.141592 ],
        ];
    }

    public  function  testSearchNull()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))
            ->configure()->withMetadata([
                "field1" => ORMType::STRING, 
                "field2" => ORMType::STRING, 
            ]);

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2'))
            ->will($this->returnSelf() )
            ;

        $e = new Expr();
        $comp1 = $e->isNull('a.field1');
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo($comp1))
            ->will($this->returnSelf() )
            ;

        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [
              "search" => [ "value" => "" ],
              "columns" => [
                   [ 'searchable' => true, 'search' => [ 'value' => 'is null' ] ],
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );

    }


    public  function  testSearchNotNull()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))
            ->configure()->withMetadata([
                "field1" => ORMType::STRING, 
                "field2" => ORMType::STRING, 
            ]);

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2'))
            ->will($this->returnSelf() )
            ;

        $e = new Expr();
        $comp1 = $e->isNotNull('a.field1');
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo($comp1))
            ->will($this->returnSelf() )
            ;

        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [
              "search" => [ "value" => "" ],
              "columns" => [
                   [ 'searchable' => true, 'search' => [ 'value' => 'is not null' ] ],
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }


    public function testSearchBetweenDate()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();
        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1','field2']))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

        $expected = [
            'field1' => ORMType::DATETIME_MUTABLE,
            'field2' => ORMType::DATETIME_MUTABLE,
        ];

        $metadataMock->expects($this->exactly(2))
            ->method('getTypeOfField')
            ->willReturnCallback(function($field) use (&$expected) {
                $this->assertArrayHasKey($field, $expected);
                $e = $expected[$field];
                // $field debe ser el primer elemento del array
                array_shift($expected);
                return $e;
            });
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2'))
            ->will($this->returnSelf() )
            ;

        $expected2 = [
            ['ppp1', '1900-01-01'],
            ['ppp2','2012-01-01'] 
        ];

        $qbMock->expects($this->exactly(2))
            ->method('setParameter')
            ->with($this->callback(function($param) use (&$expected2) {
                $e = $expected2[0];
                return $param === $e[0];
            }), $this->callback(function($value) use (&$expected2) {
                $e = $expected2[0];
                \array_shift($expected2);
                return $value === $e[1];
            }))
            ->will($this->returnSelf())
            ;

        $e = new Expr();
        if( \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder::doctrineExtensionsEnabled() ) {
            $comp1 = $e->between("DATE_FORMAT(a.field1,'YYYY-MM-DD')", ':ppp1', ':ppp2');
        } else {
            $comp1 = $e->between('a.field1', ':ppp1', ':ppp2');
        }
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo($comp1))
            ->will($this->returnSelf() )
            ;

        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [
              "search" => [ "value" => "" ],
              "columns" => [
                   [ 'searchable' => true, 'search' => [ 'value' => 'between 1900-01-01 and 2012-01-01' ] ],
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }

    public function testSearchBetweenStrings()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();
        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will($this->returnValue(['field1','field2']))
            ;

        $metadataMock->expects($this->exactly(2))
            ->method('getTypeOfField')
            ->will( $this->returnValue(ORMType::STRING))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->will($this->returnValue($metadataMock))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2'))
            ->will($this->returnSelf() )
            ;

        $expected2 = [
            ['ppp1', 'A'],
            ['ppp2', 'Z'] 
        ];

        $qbMock->expects($this->exactly(2))
            ->method('setParameter')
            ->with($this->callback(function($param) use (&$expected2) {
                $e = $expected2[0];
                return $param === $e[0];
            }), $this->callback(function($value) use (&$expected2) {
                $e = $expected2[0];
                \array_shift($expected2);
                return $value === $e[1];
            }))
            ->will($this->returnSelf())
            ;

        $e = new Expr();
        $comp1 = $e->between('a.field1', ':ppp1', ':ppp2');
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo($comp1))
            ->will($this->returnSelf() )
            ;

        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [
              "search" => [ "value" => "" ],
              "columns" => [
                   [ 'searchable' => true, 'search' => [ 'value' => 'between A and Z' ] ],
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );

    }


    public function testSearchOnBooleanColumns(): void
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

        $expected = [
            'field1' => ORMType::BOOLEAN,
            'field2' => ORMType::STRING,
        ];

        $metadataMock->expects($this->exactly(2))
            ->method('getTypeOfField')
            ->willReturnCallback(function($field) use (&$expected) {
                $this->assertArrayHasKey($field, $expected);
                $e = $expected[$field];
                // $field debe ser el primer elemento del array
                array_shift($expected);
                return $e;
            });


        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1','field2']))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2'))
            ->will($this->returnSelf() )
            ;

        $e = new Expr();
        $comp2 = $e->like('LOWER(a.field2)', ':ppp2');
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo('LOWER(a.field2) LIKE :ppp1'))
            ->will($this->returnSelf() )
            ;

        $qbMock->expects($this->exactly(1))
            ->method('setParameter')
            ->with($this->equalTo('ppp1'),$this->equalTo('%x%'),$this->anything())
            ->will($this->returnSelf())
            ;

        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ 
              "search" => [ "value" => "x" ],
              "columns" => [
                   [ 'searchable' => true ],
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );

    }

    /**
     * @dataProvider getIntConditions 
     */
    public function testSearchLtInt($condition) : void
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))
            ->configure()->withMetadata([
                "field1" => ORMType::INTEGER, 
                "field2" => ORMType::STRING, 
            ]);

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2'))
            ->will($this->returnSelf() )
            ;

        $e = new Expr();
        $comp1 = 'a.field1 ' . $condition;
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo($comp1))
            ->will($this->returnSelf() )
            ;

        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [
              "search" => [ "value" => "" ],
              "columns" => [
                   [ 'searchable' => true, 'search' => [ 'value' => $condition ] ],
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }

    public function getIntConditions()
    {
        return [
            ['> 4'],
            ['< 4'],
            ['>= 4'],
            ['<= 4'],
        ];
    }


    /**
     * @dataProvider getAllORMTypes
     */
    public function testAllORMTypes($ormType, $searchStr, $driver, $expectedSearch = null)
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock, $driver))->configure();

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

        $metadataMock->expects($this->exactly(1))
            ->method('getTypeOfField')
            ->with($this->equalTo('field1'))
            ->will($this->returnValue($ormType))
            ;

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1']))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1'))
            ->will($this->returnSelf() )
            ;

        if (is_string($expectedSearch)) {
            $qbMock->expects($this->once())
                ->method('andWhere')
                ->with($this->equalTo(new Expr\Orx($expectedSearch)))
                ->will($this->returnSelf())
                ;

            $qbMock->expects($this->once())
                ->method('setParameter')
                ->will($this->returnSelf())
                ;
        } else if (false === $expectedSearch) {
            $qbMock->expects($this->never())
                ->method('andWhere');
            $qbMock->expects($this->never())
                ->method('setParameter');
        }

        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock
            ->method('expr')
            ->will($this->returnValue(new Expr()))
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [
              "search" => [ "value" => $searchStr ],
              "columns" => [
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }

    public function getAllORMTypes():array
    {
        $ret = [];
        $reflectionClass = new \ReflectionClass(ORMType::class);
        $platforms = [
                MySQL80Platform::class,
                AbstractMySQLPlatform::class,
                MySQLPlatform::class,
                MariaDBPlatform::class,
                MariaDB1052Platform::class,
                MariaDB1060Platform::class,
                PostgreSQLPlatform::class,
                OraclePlatform::class,
        ];
        foreach($platforms as $driver ) {
            foreach( $reflectionClass->getConstants() as $constant => $values ) {
                if (is_array($values)) {
                    $values = array_keys($values);
                } else {
                    $values = [$values];
                }
                foreach ($values as $value) {
                    array_push($ret, [ $value, 'stringVal', $driver ] );
                    array_push($ret, [ $value, '33', $driver ] );
                    array_push($ret, [ $value, '33.2', $driver ] );
                    array_push($ret, [ $value, '2017-01-01', $driver ] );
                }
            }
        }

        array_push($ret, [ ORMType::JSON, 'stringVal', MySQL80Platform::class, "LOWER(a.field1) LIKE :ppp1"] );
        array_push($ret, [ ORMType::JSON, 'stringVal', MariaDBPlatform::class, "LOWER(a.field1) LIKE :ppp1"] );
        array_push($ret, [ ORMType::JSON, 'stringVal', PostgreSQLPlatform::class, false] );

        return $ret;
    }

    public function testInvalidORMType()
    {
        $this->expectException(\LogicException::class);

        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

        $metadataMock->expects($this->once())
            ->method('getTypeOfField')
            ->with( $this->equalTo('field1'))
            ->will( $this->returnValue('unknown_type'))
            ;

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1']))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1'))
            ->will($this->returnSelf() )
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [
              "search" => [ "value" => 'x' ],
              "columns" => [
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }

    public function testInvalidColumn() : void
    {
        $this->expectException(\LogicException::class);

        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();
        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1','field2']))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2, a.field3'))
            ->will($this->returnSelf() )
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [
              "search" => [ "value" => "" ],
              "columns" => [
                   [ 'searchable' => true ],
                   [ 'searchable' => true ],
                   [ 'searchable' => true, 'search' => [ 'value' => 'x' ] ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->add( 'field3' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }
}
// vim:sw=4 ts=4 sts=4 et
