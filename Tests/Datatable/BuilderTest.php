<?php

namespace  Silvioq\ReportBundle\Tests\Datatable;

use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;
use Silvioq\ReportBundle\Datatable\Builder;
use Silvioq\ReportBundle\Datatable\WhereBuilder;
use Silvioq\ReportBundle\Datatable\BuilderException;

class  BuilderTest  extends  TestCase
{
    public  function  testGetAlias()
    {
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ "search" => [ "value" => "x" ] ] );
        $dt
            ->add( 'field1' )
            ->add( 'a.field2' )
            ->from( 'Test:Table', 'a' )
            ->join( 'a.joinme', 'j' );
            ;

        $this->assertEquals( 'a', $dt->getAlias() );
        $this->assertEquals( 'Test:Table', $dt->getRepo() );
        $this->assertEquals( [ 'j' => 'a.joinme' ], $dt->getJoins() );
        $this->assertEquals( [ 'field1', 'a.field2' ], $dt->getColumns() );
    }

    public function testArrayAdd()
    {
        $emMock  = $this->getMockBuilder(\Doctrine\ORM\EntityManager::class)
                ->disableOriginalConstructor()
                ->getMock()
                ;

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ "search" => [ "value" => "x" ] ] );
        $dt
            ->add(['field1','field2'])
            ->from('Test:Table', 'a')
            ;

        $this->assertEquals( [ 'field1', 'field2' ], $dt->getColumns() );
    }

    public function testGetDrawFromDatatableJavascriptCall()
    {
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $dt = new Builder( $emMock, new WhereBuilder($emMock), [ "draw" => '4' ] );
        $this->assertEquals(4, $dt->getDraw() );
    }

    public function testThrowsOnAlreadyJoined()
    {
        $emMock = $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();
        $dt = new Builder( $emMock, new WhereBuilder($emMock), [ "search" => [ "value" => "x" ] ] );
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->join( 'field3', 'd' )
            ->from( 'Test:Table', 'a' );

        $dt->join( 'field4', 'c' );
        $this->expectException( BuilderException::class );
        $dt->join( 'field4', 'c' );
    }

    public function testThrowsOnAlreadyAddedColumn()
    {
        $emMock = $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $dt = new Builder( $emMock, new WhereBuilder($emMock), [ ] );
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' );

        $dt->add( 'field3' );
        $this->expectException( BuilderException::class );
        $dt->add( 'field3' );
    }


    public function testThrowsOnNoDeclaredRepo()
    {
        $emMock = $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $dt = new Builder($emMock, new WhereBuilder($emMock), [ ]);
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ;

        $this->expectException( BuilderException::class );
        $dt->getArray();
    }
}

