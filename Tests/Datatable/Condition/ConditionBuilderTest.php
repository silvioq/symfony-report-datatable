<?php

namespace  Silvioq\ReportBundle\Tests\Datatable\Condition;

use Silvioq\ReportBundle\Datatable\Condition\ConditionBuilder;
use Silvioq\ReportBundle\Datatable\Builder as DatatableBuilder;
use Silvioq\ReportBundle\Datatable\WhereBuilder;
use Doctrine\ORM\QueryBuilder;

use PHPUnit\Framework\TestCase;

class ConditionBuilderTest extends TestCase
{
    /**
     * @dataProvider getConfiguredMock
     * @depends testLoadClass
     */
    public function testBuilder($data)
    {
        $reader = new \Doctrine\Common\Annotations\AnnotationReader();

        /** @var DatatableBuilder|\PHPUnit\Framework\MockObject\MockObject */
        $mockDt = $this->getMockBuilder(DatatableBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockQb = $this->getMockBuilder(QueryBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();

        $whereBuilder = $this->getWhereMock();

        $function = null;
        $mockDt->expects($this->once())
            ->method('condition')
            ->with($this->callback(function($data) use (&$function) {
                $function = $data;
                return is_callable($function);
            }));
            ;

        $whereBuilder->expects($this->once())
            ->method('getExpresiveWhere')
            ->with("a.column","one")
            ->willReturn("condition");

        $builder = new ConditionBuilder($reader, $whereBuilder);
        $builder->configureCondition($data, $mockDt);

        $function($mockQb);
    }

    public function getConfiguredMock() : iterable
    {
        yield 'Annotations' => [new Mock\MockAnnotationClass(['column' => 'one'])];
        yield 'Attributes' => [new Mock\MockAttributeClass(['column' => 'one'])];
    }

    public function testLoadClass()
    {
        $reader = new \Doctrine\Common\Annotations\AnnotationReader();
        $whereBuilder = $this->getWhereMock();
        $builder = new ConditionBuilder($reader, $whereBuilder);
        $configurator = $builder->loadConfigurator(Mock\MockAttributeClass::class);
        $this->assertSame(["columnName" => "a.column", "type" => "like", "callback" => null], $configurator->get("column"));

        $configurator = $builder->loadConfigurator(Mock\MockAnnotationClass::class);
        $this->assertSame(["columnName" => "a.column", "type" => "like", "callback" => null], $configurator->get("column"));
    }

    /**
     * @depends testLoadClass
     */
    public function testBuilderRootAlias()
    {
        $reader = new \Doctrine\Common\Annotations\AnnotationReader();
        $data = new Mock\MockAttributeClass(['column4' => 'one']);

        /** @var DatatableBuilder|\PHPUnit\Framework\MockObject\MockObject */
        $mockDt = $this->getMockBuilder(DatatableBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockQb = $this->getMockBuilder(QueryBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $whereBuilder = $this->getWhereMock();
        $mockExpr = $this->getMockBuilder(\Doctrine\ORM\Query\Expr::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockComp = $this->getMockBuilder(\Doctrine\ORM\Query\Expr\Comparison::class)
                ->disableOriginalConstructor()
                ->getMock();

        $function = null;
        $mockDt->expects($this->once())
            ->method('condition')
            ->with($this->callback(function($data) use (&$function) {
                $function = $data;
                return is_callable($function);
            }));
            ;

        $whereBuilder->expects($this->never())
            ->method('getExpresiveWhere');

        $mockQb->expects($this->once())
            ->method('getRootAlias')
            ->with()
            ->willReturn('x');

        $mockQb->expects($this->once())
            ->method('expr')
            ->with()
            ->willReturn($mockExpr);

        $mockExpr->expects($this->once())
            ->method('eq')
            ->with('x.column4', ':ppp')
            ->will($this->returnValue($mockComp))
            ;
        $mockQb->expects($this->never())
            ->method('setParameter')
            ->with(':ppp', 'one')
            ;

        $whereBuilder->expects($this->exactly(1))
            ->method("createParameter")
            ->willReturn(":ppp");
        $builder = new ConditionBuilder($reader, $whereBuilder);
        $builder->configureCondition($data, $mockDt);

        $function($mockQb);
    }

    /**
     * @depends testLoadClass
     */
    public function testBuilderInArray()
    {
        $reader = new \Doctrine\Common\Annotations\AnnotationReader();
        $data = new Mock\MockAttributeClass(['column2' => ['one', 'two']]);

        /** @var DatatableBuilder|\PHPUnit\Framework\MockObject\MockObject */
        $mockDt = $this->getMockBuilder(DatatableBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockQb = $this->getMockBuilder(QueryBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $whereBuilder = $this->getWhereMock();
        $mockExpr = $this->getMockBuilder(\Doctrine\ORM\Query\Expr::class)
                ->disableOriginalConstructor()
                ->getMock();

    	$mockFunc = $this->getMockBuilder(\Doctrine\ORM\Query\Expr\Func::class)
                ->disableOriginalConstructor()
                ->getMock();

        $function = null;
        $mockDt->expects($this->once())
            ->method('condition')
            ->with($this->callback(function($data) use (&$function) {
                $function = $data;
                return is_callable($function);
            }));
            ;

        $whereBuilder->expects($this->never())
            ->method('getExpresiveWhere');

        $whereBuilder->expects($this->exactly(1))
            ->method("createParameter")
            ->willReturn(":ppp");

        $mockQb->expects($this->once())
            ->method('expr')
            ->with()
            ->willReturn($mockExpr);

        $mockExpr->expects($this->once())
            ->method('in')
            ->with('a.column2', ':ppp')
            ->will($this->returnValue($mockFunc))
            ;
        $builder = new ConditionBuilder($reader, $whereBuilder);
        $builder->configureCondition($data, $mockDt);

        $function($mockQb);
    }

    /**
     * @depends testLoadClass
     */
    public function testBuilderInCondition()
    {
        $reader = new \Doctrine\Common\Annotations\AnnotationReader();
        $data = new Mock\MockAttributeClass(['column5' => ['one', 'two']]);

        /** @var DatatableBuilder|\PHPUnit\Framework\MockObject\MockObject */
        $mockDt = $this->getMockBuilder(DatatableBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockQb = $this->getMockBuilder(QueryBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $whereBuilder = $this->getWhereMock();
        $mockExpr = $this->getMockBuilder(\Doctrine\ORM\Query\Expr::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockComp = $this->getMockBuilder(\Doctrine\ORM\Query\Expr\Comparison::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockOr = $this->getMockBuilder(\Doctrine\ORM\Query\Expr\Orx::class)
                ->disableOriginalConstructor()
                ->getMock();

        $function = null;
        $mockDt->expects($this->once())
            ->method('condition')
            ->with($this->callback(function($data) use (&$function) {
                $function = $data;
                return is_callable($function);
            }));
            ;

        $whereBuilder->expects($this->never())
            ->method('getExpresiveWhere');

        $whereBuilder->expects($this->exactly(2))
            ->method("createParameter")
            ->willReturn(":ppp");

        $mockQb->expects($this->any())
            ->method('expr')
            ->with()
            ->willReturn($mockExpr);

        $mockExpr->expects($this->once())
            ->method('orX')
            ->will($this->returnValue($mockOr))
            ;
        $mockExpr->expects($this->exactly(2))
            ->method('isMemberOf')
            ->with(':ppp', 'a.column5')
            ->will($this->returnValue($mockComp))
            ;
        $builder = new ConditionBuilder($reader, $whereBuilder);
        $builder->configureCondition($data, $mockDt);

        $function($mockQb);
    }

    /**
     * @depends testLoadClass
     */
    public function testBuilderOtherClass()
    {
        $reader = new \Doctrine\Common\Annotations\AnnotationReader();
        $data = ['column' => 'one'];

        /** @var DatatableBuilder|\PHPUnit\Framework\MockObject\MockObject */
        $mockDt = $this->getMockBuilder(DatatableBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockQb = $this->getMockBuilder(QueryBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();

        $mockExpr = $this->getMockBuilder(\Doctrine\ORM\Query\Expr::class)
                ->disableOriginalConstructor()
                ->getMock();

        $whereBuilder = $this->getWhereMock();

        $function = null;
        $mockDt->expects($this->once())
            ->method('condition')
            ->with($this->callback(function($data) use (&$function) {
                $function = $data;
                return is_callable($function);
            }));
            ;

        $whereBuilder->expects($this->once())
            ->method('getExpresiveWhere')
            ->with("a.column","one")
            ->willReturn("condition");


        $builder = new ConditionBuilder($reader, $whereBuilder);
        $builder->configureCondition($data, $mockDt, Mock\MockAttributeClass::class);

        $function($mockQb);
    }

    public function testLikeWithArray()
    {
        $reader = new \Doctrine\Common\Annotations\AnnotationReader();
        $data = new Mock\MockAttributeClass(['column' => ['one', 'two']]);

        /** @var DatatableBuilder|\PHPUnit\Framework\MockObject\MockObject */
        $mockDt = $this->getMockBuilder(DatatableBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockQb = $this->getMockBuilder(QueryBuilder::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockExpr = $this->getMockBuilder(\Doctrine\ORM\Query\Expr::class)
                ->disableOriginalConstructor()
                ->getMock();

        $mockExprOr = $this->getMockBuilder(\Doctrine\ORM\Query\Expr\Orx::class)
                ->disableOriginalConstructor()
                ->getMock();

        $whereBuilder = $this->getWhereMock();
        $mockQb->expects($this->once())
            ->method('expr')
            ->with()
            ->willReturn($mockExpr);

        $function = null;
        $mockDt->expects($this->once())
            ->method('condition')
            ->with($this->callback(function($data) use (&$function) {
                $function = $data;
                return is_callable($function);
            }));
            ;

        $mockExpr->expects($this->once())
            ->method("orX")
            ->willReturn($mockExprOr);
        $mockExprOr->expects($this->exactly(2))
            ->method("add")
            ->with("condition");

        $whereBuilder->expects($this->exactly(2))
            ->method('getExpresiveWhere')
            ->withConsecutive(["a.column","one"], ["a.column","two"])
            ->willReturn("condition");

        $builder = new ConditionBuilder($reader, $whereBuilder);
        $builder->configureCondition($data, $mockDt);

        $function($mockQb);
    }


    private function getWhereMock() : WhereBuilder|\PHPUnit\Framework\MockObject\MockObject
    {
        return $this->getMockBuilder(WhereBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
