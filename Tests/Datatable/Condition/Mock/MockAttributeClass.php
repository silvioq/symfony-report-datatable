<?php

namespace Silvioq\ReportBundle\Tests\Datatable\Condition\Mock;

use Silvioq\ReportBundle\Attribute\Datatable\ConditionDefinition;
use Silvioq\ReportBundle\Attribute\Datatable\ConditionDataMethod;

#[ConditionDefinition(column: "a.column", filter: "column")]
#[ConditionDefinition(column: "a.column2", filter: "column2", type: "eq")]
#[ConditionDefinition(column: "column4", filter: "column4", type: "eq")]
#[ConditionDefinition(column: "a.column5", filter: "column5", type: "in")]
#[ConditionDataMethod("getArrayData")]
class MockAttributeClass
{
    #[ConditionDefinition(column: "a.column3", filter: "column3")]
    public function filterMe($queryBuilder)
    {
    }

    private $data;
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function getArrayData(): array
    {
        return $this->data;
    }
}