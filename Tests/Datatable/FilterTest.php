<?php

namespace  Silvioq\ReportBundle\Tests\Datatable;

use PHPUnit\Framework\TestCase;
use Silvioq\ReportBundle\Datatable\Builder;
use Silvioq\ReportBundle\Datatable\BuilderException;
use Silvioq\ReportBundle\Datatable\WhereBuilder;
use Doctrine\DBAL\Types\Types as ORMType;
use Doctrine\ORM\EntityManager;

class FilterTest extends TestCase
{
    public  function  testResultFiltering()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(EntityManager::class,
               array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();
        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock(\Doctrine\ORM\QueryBuilder::class, ["select", "getQuery"], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );
        
        $result = [
            [ 'field1' => 1, 'field2' => 1],
            [ 'field1' => 2, 'field2' => 2],
            [ 'field1' => 3, 'field2' => 3]
        ];
        
        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;
            
        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;
        
        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;
            
        $qbMock->expects($this->once())
            ->method('select')
            ->will($this->returnSelf() )
            ;
            
        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;
            
        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue($result))
            ;

        $expected = [
            'field1' => ORMType::INTEGER,
            'field2' => ORMType::INTEGER
        ];

        $metadataMock->expects($this->exactly(2))
            ->method('getTypeOfField')
            ->willReturnCallback(function($field) use ($expected) {
                $this->assertArrayHasKey($field, $expected);
                $e = $expected[$field];
                // $field debe ser el primer elemento del array
                array_shift($expected);
                return $e;
            });

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1','field2']))
            ;

            
        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ "search" => [ "value" => "x" ] ] );
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ->filter( 'field2', 
                function($val){ return $val["field2"] * 2; } )
            ;

        $this->assertEquals( $dt->getArray(), [
                [ 1, 1 * 2],
                [ 2, 2 * 2],
                [ 3, 3 * 2]
            ] );

    }
    
    public function testThrowsWhenNotExistsColumn()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock = $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

         $dt = new Builder( $emMock, new WhereBuilder($emMock), [ "search" => [ "value" => "x" ] ] );
         $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' );

         $this->expectException( BuilderException::class );
         $dt->filter( 'field3', 'get_class' );
    }

    public function testThrowsWhenFilterIsAlreadyDefined()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock = $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $dt = new Builder( $emMock, new WhereBuilder($emMock), [ "search" => [ "value" => "x" ] ] );
        $dt
           ->add( 'field1' )
           ->add( 'field2' )
           ->from( 'Test:Table', 'a' );

        $dt->filter( 'field2', 'get_class' );
        $this->expectException( BuilderException::class );
        $dt->filter( 'field2', 'get_class' );
    }

    public function testThrowsWhenFilterFunctionIsInvalid()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock = $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

         $dt = new Builder( $emMock, new WhereBuilder($emMock), [ "search" => [ "value" => "x" ] ] );
         $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' );

         $this->expectException( \InvalidArgumentException::class );
         $dt->filter( 'field2', 'none' );
    }
}
