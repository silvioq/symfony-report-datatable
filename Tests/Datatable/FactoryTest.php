<?php

namespace  Silvioq\ReportBundle\Tests\Datatable;

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Silvioq\ReportBundle\Datatable\DatatableFactory;
use Silvioq\ReportBundle\Datatable\Builder;

class  FactoryTest  extends  TestCase
{
    private $requestStack;

    public  function  setup() : void
    {
        $request = new Request( 
                [], // GET
                [], // POST
                [], // ?
                [], // COOKIE
                [], // FILES
                []  // SERVER
            );
        $this->requestStack = new RequestStack();
        $this->requestStack->push( $request );
    }

    public  function  testFactory()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
                array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $factory = new DatatableFactory( $emMock, $this->requestStack );
        $this->assertInstanceOf( Builder::class, $factory->buildDatatable() );
    }

    public  function  testFactoryNoRequest()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
                array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        /** @var \PHPUnit\Framework\MockObject\MockObject|RequestStack */
        $requestStack = $this->getMockBuilder(RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock()
            ;

        $requestStack->expects($this->once())
            ->method('getCurrentRequest')
            ->willReturn(null)
            ;

        $factory = new DatatableFactory($emMock, $requestStack);
        $this->assertInstanceOf( Builder::class, $factory->buildDatatable() );
    }
}
