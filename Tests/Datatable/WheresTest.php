<?php

namespace  Silvioq\ReportBundle\Tests\Datatable;

use PHPUnit\Framework\TestCase;
use Doctrine\ORM\QueryBuilder;
use Silvioq\ReportBundle\Datatable\Builder;
use Silvioq\ReportBundle\Datatable\WhereBuilder;

class WheresTest extends TestCase
{
    /**
     * @dataProvider whereFunctions
     */
    function testAndWhereOnQueryBuilder($functionName)
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))
            ->configure()->withMetadata();

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere"], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;
            
        $qbMock->expects($this->once())
            ->method('select')
            ->will($this->returnSelf() )
            ;
            
        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;
        
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo( 'a.field1 = 1') )
            ;
            
        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;
            
        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ "search" => [ "value" => "x" ] ] );
        $dt
            ->add( 'field1' )
            ->add( 'a.field2' )
            ->from( 'Test:Table', 'a' )
            ->$functionName( 'a.field1 = 1')
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }



    /**
     * @dataProvider whereFunctions
     */
    function testAndWhereWithCallable($functionName)
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);
        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( QueryBuilder::class, ["select", "getQuery", "andWhere"], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))
            ->configure()->withMetadata();

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;
            
        $qbMock->expects($this->once())
            ->method('select')
            ->will($this->returnSelf() )
            ;
            
        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;
        
        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with( "1 = 1" )
            ;
            
        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;
            
        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ "search" => [ "value" => "x" ] ] );
            
            
        $called = 0;
        $_self = $this;
        
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ->$functionName( function( $qb) use( &$called, $_self ){
                $called ++;
                $_self->assertInstanceOf( QueryBuilder::class, $qb );
                return "1 = 1";
            } )
            ;

        $this->assertEquals( $dt->getArray(), [] );
        $this->assertEquals( 1, $called );
    }

    /**
     * @dataProvider whereFunctions
     */
    function testAndWhereWithCallableWithoutResult($functionName)
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))
            ->configure()->withMetadata();

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( QueryBuilder::class, ["select", "getQuery", "andWhere", "leftJoin"], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        
        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;
            
        $qbMock->expects($this->once())
            ->method('select')
            ->will($this->returnSelf() )
            ;
            
        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;
        
        $qbMock->expects($this->never())
            ->method('andWhere')
            ;

        $qbMock->expects($this->once())
            ->method('leftJoin')
            ;
            
        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([]))
            ;
            
        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ "search" => [ "value" => "x" ] ] );
            
            
        $called = 0;
        $_self = $this;
        
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ->$functionName( function( $qb) use( &$called, $_self ){
                $called ++;
                $_self->assertInstanceOf( QueryBuilder::class, $qb );
                $qb->leftJoin("Test:Table2", "b" );
            } )
            ;

        $this->assertEquals( $dt->getArray(), [] );
        $this->assertEquals( 1, $called );
    }
    

    function testAndWhereCalledFromCount()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere", 'setMaxResults'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;
            
        $qbMock->expects($this->once())
            ->method('select')
            ->will($this->returnSelf() )
            ;

        $qbMock->expects($this->once())
            ->method('setMaxResults')
            ->with($this->equalTo( '1') )
            ->will($this->returnSelf())
            ;
            
        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $qbMock->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo( 'a.field1 = 1') )
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([[0,3]]))
            ;
            
        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ "search" => [ "value" => "x" ] ] );
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ->where( 'a.field1 = 1')
            ->condition( 'a.field2 = 2' )
            ;

        $this->assertEquals( 3, $dt->getCount() );
    }

    function testAndWhereCalledFromFilteredCount()
    {
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);

        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))
            ->configure()->withMetadata();

        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere", 'setMaxResults'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->will($this->returnSelf() )
            ;

        $qbMock->expects($this->exactly(0))
            ->method('setMaxResults')
            ->with($this->equalTo( '1') )
            ->will($this->returnSelf())
            ;

        $qbMock->expects($this->once())
            ->method('getQuery')
            ->will( $this->returnValue($queryMock) )
            ;

        $expected = [
            'a.field2 = 2',
            'a.field1 = 1',
        ];
        $qbMock->expects($this->exactly(2))
            ->method('andWhere')
            ->with($this->callback(function($arg) use(&$expected) {
                $e = \array_shift($expected);
                return $e == $arg;
            }))
            ->willReturnSelf()
            ;

        $queryMock->expects($this->once())
            ->method('getResult')
            ->will( $this->returnValue([[0,3]]))
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [ "search" => [ "value" => "x" ] ] );
        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->from( 'Test:Table', 'a' )
            ->where( 'a.field1 = 1')
            ->condition( 'a.field2 = 2' )
            ;

        $this->assertEquals( 3, $dt->getFilteredCount() );
    }




    public function whereFunctions()
    {
        return[ ["where"], ["condition"] ];
    }

    public function testInvalidColumnOnGlobalSearch()
    {
        $this->expectException(\LogicException::class);
        
        /** @var \PHPUnit\Framework\MockObject\MockObject|\Doctrine\ORM\EntityManagerInterface */
        $emMock  = $this->createMock(\Doctrine\ORM\EntityManager::class,
               array('getRepository', 'getClassMetadata'), array(), '', false);
        (new \Silvioq\ReportBundle\Tests\MockBuilder\ConfigurationMockBuilder($this,$emMock))->configure();
        $repoMock = $this->createMock(\Doctrine\ORM\EntityRepository::class, ["createQueryBuilder"], array(), '', false );
        $qbMock = $this->createMock( \Doctrine\ORM\QueryBuilder::class, ["select", "getQuery", "andWhere",'expr'], array(), '', false );
        $queryMock = $this->createMock(\Doctrine\ORM\Query::class, ['getResult'], array(), '', false );
        $metadataMock = $this->createMock(\Doctrine\ORM\Mapping\ClassMetadata::class, ['getFieldNames','getTypeOfField'], array(), '', false );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($repoMock))
            ;

        $metadataMock->expects($this->once())
            ->method('getFieldNames')
            ->will( $this->returnValue(['field1','field2']))
            ;

        $emMock->expects($this->once())
            ->method('getClassMetadata')
            ->with($this->equalTo('Test:Table'))
            ->will($this->returnValue($metadataMock))
            ;

        $repoMock->expects($this->once())
            ->method("createQueryBuilder")
            ->with($this->equalTo('a'))
            ->will($this->returnValue( $qbMock ) )
            ;

        $qbMock->expects($this->once())
            ->method('select')
            ->with($this->equalTo('a.field1, a.field2, a.field3'))
            ->will($this->returnSelf() )
            ;

        $dt = new Builder( $emMock, new WhereBuilder($emMock),
            [
              "search" => [ "value" => "x" ],
              "columns" => [
                   [ 'searchable' => true ],
                   [ 'searchable' => true ],
                   [ 'searchable' => true ],
                ],
            ] );

        $dt
            ->add( 'field1' )
            ->add( 'field2' )
            ->add( 'field3' )
            ->from( 'Test:Table', 'a' )
            ;

        $this->assertEquals( $dt->getArray(), [] );
    }
}
