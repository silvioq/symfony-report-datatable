<?php

namespace  Silvioq\ReportBundle\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\ReportBundle\SilvioqReportBundle;
use Silvioq\ReportBundle\DependencyInjection\Compiler\AddTableLoaderPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SilvioqReportBundleTest extends TestCase
{
    public function testBuild()
    {
        $containerMock = $this->getMockBuilder(ContainerBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $b = new SilvioqReportBundle();
        $containerMock->expects($this->once())
            ->method("addCompilerPass")
            ->with($this->isInstanceOf(AddTableLoaderPass::class))
            ;

        $b->build($containerMock);
    }
}
