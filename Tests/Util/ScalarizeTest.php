<?php

namespace  Silvioq\ReportBundle\Tests\Util;

use PHPUnit\Framework\TestCase;
use Silvioq\ReportBundle\Util\Scalarize;

class ScalarizeTest extends TestCase
{

    public function testScalars()
    {
        $this->assertEquals( 1, Scalarize::toScalar( 1 ) );
        $this->assertEquals( '', Scalarize::toScalar( null ) );
        $this->assertEquals( 'hello w', Scalarize::toScalar( "hello w" ) );
        $this->assertTrue( Scalarize::toScalar( true ) );
    }

    public function testDateTime()
    {
        $date = new \DateTime();
        $date->setDate( 1945,10,17 );
        $date->setTime( 0,0, 0 );
        $this->assertEquals( '1945-10-17', Scalarize::toScalar( $date ) );
        $this->assertEquals( '17/10/1945', (new Scalarize(['date_format' => 'd/m/Y']))->scalarize($date) );
    }

    public function testDateTimeWithCallback()
    {
        $date = new \DateTime();
        $date->setDate( 1945,10,17 );
        $date->setTime( 0,0, 0 );
        $this->assertEquals( '1945-10-17', Scalarize::toScalar( $date ) );
        $callback = function($dateTime) {
            return $dateTime->format('Ymd');
        };
        $this->assertSame('19451017', (new Scalarize(['date_format' => $callback]))->scalarize($date));
    }

    public function testArray()
    {
        $this->assertEquals( '1,2,name', Scalarize::toScalar( [ 1, 2, 'name' ] ) );
        $arr = [ 'a' => 1, 'b' => 2, 'name' => 'name' ];
        $this->assertEquals( '1,2,name', Scalarize::toScalar( $arr ) );

        $this->assertEquals( '1-2-name', (new Scalarize(['array_separator' => '-' ]))->scalarize($arr) );
    }

    public function testObject()
    {
        $mock = $this->getMockBuilder(stdClass::class)
                ->disableOriginalConstructor()
                ->setMethods(['__toString'])
                ->getMock();

        $mock->expects($this->once())
            ->method('__toString')
            ->will($this->returnValue('hello'));

        $this->assertEquals( 'hello', Scalarize::toScalar( $mock ) );
    }

    public function testIteration()
    {
        $i = $this->iterateme();
        $this->assertEquals( '1,2,hello', Scalarize::toScalar($i ) );
    }

    public function testCallable()
    {
        $this->assertSame('42', Scalarize::toScalar(function(){ return '42';}));
    }

    /**
     * @dataProvider getLocaleData
     */
    public function testLocaleFormat($locale, $value, $expected)
    {
        $this->assertSame($expected, (new Scalarize(['locale_format' => $locale]))->scalarize($value));
    }

    public function getLocaleData() : array
    {
        return [
            ["es_AR", 10, '10'],
            ["es_AR", 10.0, '10,00'],
            ["es_AR", 1000, '1.000'],
            ["es_AR", 6174, '6.174'],
            ["es_AR", 3.141592, '3,14'],
        ];
    }

    public function testConfig()
    {
        $this->assertNotNull( $scalar = new Scalarize( ) );
        $this->assertNotNull( $scalar = new Scalarize( ['array_separator' => '|' ] ) );
        $this->assertNotNull( $scalar = new Scalarize( ['date_format' => 'Y-m' ] ) );
    }

    public function testInvalidConfig()
    {
        $this->expectException(\Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException::class);
        new Scalarize(['not_valid_option' => false]);
    }

    /**
     * @dataProvider getFloatValues()
     */
    public function testFormatFloat($floatFormatOption, $value, $expected)
    {
        $scalarize = new Scalarize(["float_format" => $floatFormatOption]);
        $this->assertSame($expected, $scalarize->scalarize($value));
    }

    public function getFloatValues()
    {
        $numberFormatter = new \NumberFormatter("es_AR", \NumberFormatter::DECIMAL);
        $numberFormatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, 2);
        $numberFormatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, 2);
 
        return [
            [$numberFormatter, 10.3, '10,30'],
            [$numberFormatter, 1000.0, '1.000,00'],
            [$numberFormatter, 10.0, '10,00'],
            [$numberFormatter, 1000.0, '1.000,00'],
            [$numberFormatter, 1000, 1000], // si no es float, no cambia nada
            [$numberFormatter, '1000.0', '1000.0'], // si no es float, no cambia nada
            [function ($data) {
                    return number_format($data,2);
                }, 20.0, '20.00'],
        ];
    }

    /**
     * @dataProvider getIntValues()
     */
    public function testFormatInt($formatOption, $value, $expected)
    {
        $scalarize = new Scalarize(["integer_format" => $formatOption]);
        $this->assertSame($expected, $scalarize->scalarize($value));
    }

    public function getIntValues()
    {
        $numberFormatter = new \NumberFormatter("es_AR", \NumberFormatter::DECIMAL);
        $numberFormatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, 2);
        $numberFormatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, 2);
 
        return [
            [$numberFormatter, 10, '10,00'],
            [$numberFormatter, 1000, '1.000,00'],
            [$numberFormatter, 10, '10,00'],
            [$numberFormatter, 1000, '1.000,00'],
            [$numberFormatter, 1000.0, 1000.0], // si no es int, no cambia nada
            [$numberFormatter, '1000.0', '1000.0'], // si no es int, no cambia nada
            [function ($data) {
                    return number_format($data,2);
                }, 20, '20.00'],
        ];
    }

    private function iterateme()
    {
        yield 1;
        yield 2;
        
        $mock = $this->getMockBuilder(stdClass::class)
                ->disableOriginalConstructor()
                ->setMethods(['__toString'])
                ->getMock();

        $mock->expects($this->once())
            ->method('__toString')
            ->will($this->returnValue('hello'));

        yield $mock;
    }

}
// vim:sw=4 ts=4 sts=4 et
