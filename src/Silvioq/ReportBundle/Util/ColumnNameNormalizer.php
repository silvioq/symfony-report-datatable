<?php

namespace Silvioq\ReportBundle\Util;

/**
 * @author silvioq
 */
class ColumnNameNormalizer
{
    /**
     * Normalize column name
     */
    static public function normalizeColName(string $colName):string
    {
        return  \str_replace('.', '_', $colName);
    }
}

