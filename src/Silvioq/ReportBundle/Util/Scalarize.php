<?php

namespace Silvioq\ReportBundle\Util;

use Symfony\Component\OptionsResolver\OptionsResolver;
use NumberFormatter;

/**
 * @author silvioq
 *
 * Transform in scalar anything
 */
class Scalarize
{
    private string $arraySeparator;

    /**
     * @var string|callable
     */
    private $dateFormat;

    /**
     * @var NumberFormatter|callable|bool
     */
    private $integerFormat;

    /**
     * @var NumberFormatter|callable|bool
     */
    private $floatFormat;

    /**
     * @param array<string, scalar|callable|NumberFormatter> $config  Config settings
     *
     * Default settings:
     *
     * - array_separator. Value ","
     * - date_format. Value 'Y-m-d'
     * - integer_format.
     * - float_format.
     * - locale_format (ej. es_AR)
     */
    public function __construct(array $config = array())
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults( [
            'array_separator' => ',',
            'date_format' => 'Y-m-d',
            'spreadsheet_support' => false,
            'integer_format' => false,
            'float_format' => false,
            'locale_format' => false,
        ]);

        $resolver->setAllowedTypes('spreadsheet_support', ['boolean']);
        $resolver->setAllowedTypes('date_format', ['string', 'callable']);
        $resolver->setAllowedTypes('integer_format', ['NumberFormatter', 'callable', 'boolean']);
        $resolver->setAllowedTypes('float_format', ['NumberFormatter', 'callable', 'boolean']);
        $resolver->setAllowedTypes('locale_format', ['string', 'boolean']);

        /** @var array<string, mixed> */
        $options = $resolver->resolve($config);

        $this->arraySeparator = \is_string($options['array_separator']) ? $options['array_separator'] : ',';
        $this->dateFormat = $options['date_format'];
        if (is_string($options['locale_format'])) {
            $this->integerFormat = NumberFormatter::create($options['locale_format'], NumberFormatter::DECIMAL);
            $this->floatFormat = NumberFormatter::create($options['locale_format'], NumberFormatter::DECIMAL);
            $this->floatFormat->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS,2);
            $this->floatFormat->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS,2);
        } else {
            $this->integerFormat = $options['integer_format'];
            $this->floatFormat = $options['float_format'];
        }
    }

    /**
     * @param mixed $data
     *
     * @return scalar
     */
    public function scalarize($data)
    {
        if (null === $data) return '';

        if ($this->integerFormat && is_int($data)) {
            if ($this->integerFormat instanceof NumberFormatter) {
                return $this->integerFormat->format($data);
            } else if(is_callable($this->integerFormat)) {
                return ($this->integerFormat)($data);
            }
        }

        if ($this->floatFormat && is_float($data)) {
            if ($this->floatFormat instanceof NumberFormatter) {
                return $this->floatFormat->format($data);
            } else if(is_callable($this->floatFormat)) {
                return ($this->floatFormat)($data);
            }
        }

        if (is_string($data) || is_bool($data) || is_numeric($data))
            return $data;

        if ($data instanceof \DateTime) {
            return is_callable($this->dateFormat) ? ($this->dateFormat)($data) : $data->format($this->dateFormat);
        }

        if (is_object($data) && method_exists($data, '__toString'))
            return (string)$data;

        if( $data instanceof \Traversable)  {
            $ret = [];
            foreach( $data as $x) {
                $ret[] = static::toScalar($x); 
            }
            return join($this->arraySeparator, $ret);
        }

        // scalarize function.
        if (is_callable($data)) {
            return $this->scalarize($data());
        }

        // still an object
        if (is_object($data)) return sprintf('object(%s)', get_class($data));
        if (is_array($data)) return join($this->arraySeparator, $data);

        return '';
    }

    /**
     * @param mixed $data
     *
     * @return scalar
     */
    static public function toScalar($data)
    {
        return (new self())->scalarize($data);
    }

}
// vim:sw=4 ts=4 sts=4 et
