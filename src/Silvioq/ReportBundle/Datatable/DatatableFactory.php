<?php

namespace  Silvioq\ReportBundle\Datatable;

use Silvioq\ReportBundle\Datatable\Builder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;


class DatatableFactory
{
    /** @var EntityManagerInterface */
    private   $em;

    /** @var RequestStack */
    private   $requestStack;

    /** @var WhereBuilder|null */
    private $whereBuilder = null;

    public  function  __construct(EntityManagerInterface $em, RequestStack $requestStack )
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    /**
     * Returns a configured intance of \Silvioq\ReportBundle\Datatable\Builder
     *
     * @return Builder
     */
    public  function  buildDatatable():Builder
    {
        $currentRequest = $this->requestStack->getCurrentRequest( );
        if ($currentRequest) {
            $array = $currentRequest->query->all() + $currentRequest->request->all();
        } else {
            $array = [];
        }
        return  new  Builder($this->em, $this->buildWherebuilder(), $array);
    }

    /**
     * Return whereBuilder
     */
    public function buildWherebuilder(): WhereBuilder
    {
        if (null === $this->whereBuilder) {
            $this->whereBuilder = new WhereBuilder($this->em);
        }

        return $this->whereBuilder;
    }

}
// vim:sw=4 ts=4 sts=4 et
