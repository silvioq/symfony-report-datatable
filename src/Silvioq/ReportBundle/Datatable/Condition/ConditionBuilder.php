<?php

namespace Silvioq\ReportBundle\Datatable\Condition;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\QueryBuilder;
use Silvioq\ReportBundle\Datatable\Builder as DatatableBuilder;
use Silvioq\ReportBundle\Datatable\WhereBuilder;


/**
 * Datatable Configurator builder
 */
class ConditionBuilder
{
    private readonly Reader $reader;
    private readonly WhereBuilder $wb;

    public function __construct(Reader $reader, WhereBuilder $wb)
    {
        $this->reader = $reader;
        $this->wb = $wb;
    }

    /**
     * @param mixed $element
     */
    public function configure(ConditionConfigurator $configurator, DatatableBuilder $dataTable, $element) : void
    {
        if (is_array($element)) {
            $data = $element;
        } else {
            $method = $configurator->getMethod();
            if (null === $method && method_exists($element, "getData")){
                $method = "getData";
            }
            if (null === $method) {
                throw new \LogicException(sprintf('Not defined method for retrieve data in %s.', get_class($element)));
            }

            $data = $element->$method();

            if (!is_array($data)) {
                throw new \LogicException(sprintf('Method %s::%s must return array.', get_class($element), $method));
            }
        }

        $dataTable->condition(function (QueryBuilder $queryBuilder) use($configurator, $data, $element) {
            foreach ($data as $field => $value) {
                if (null === $value ||
                    (is_string($value) && '' === trim($value)) ||
                    (is_array($value) && 0 == count($value)) ||
                    ($value instanceof \Countable && count($value) == 0)
                    ) {
                    continue;
                }

                $def = $configurator->get($field);
                $column = $this->sanitizeColumnName($queryBuilder, $def['columnName']);

                switch ($def['type']) {
                    case 'like':
                        $this->addConditionLike($queryBuilder, $column, $value);
                        break;
                    case 'eq':
                        $this->addConditionEq($queryBuilder, $column, $value);
                        break;
                    case 'in':
                        $this->addConditionMember($queryBuilder, $column, $value);
                        break;
                    default:
                        if ($def['callback'] instanceof \ReflectionMethod) {
                            $def['callback']->invoke($element, $queryBuilder, $value);
                        } else if (is_callable($def['callback'])) {
                            $def['callback']($queryBuilder, $value);
                        } else if (is_string($def["type"])){
                            throw new \RuntimeException(sprintf('Type "%s" is not implemented yet.', $def["type"]));
                        } else {
                            throw new \RuntimeException('Invalid or not implmented type');
                        }
                }
            }
        });
    }

    /**
     * @param object $element
     * @param class-string|null $className
     */
    public function configureCondition($element, DatatableBuilder $dataTable, string $className = null) : void
    {
        if (null === $className) {
            $className = \get_class($element);
        }
        $this->configure($this->loadConfigurator($className), $dataTable, $element);
    }

    /**
     * Dada una clase, retorna el configurador.
     *
     * @param class-string $className
     *
     * @return ConditionConfigurator
     */
    public function loadConfigurator(string $className) : ConditionConfigurator
    {
        return ConditionConfigurator::loadFromClass($this->reader, $className);
    }

    private function addConditionLike(QueryBuilder $queryBuilder, string $column, mixed $value) : void
    {
        if (is_iterable($value)) {
            $orx = $queryBuilder->expr()->orX();
            foreach ($value as $v) {
                $expr = $this->wb->getExpresiveWhere($column, (string)$v);
                $orx->add($expr);
            }
            $expr = $orx;
        } else {
            // No searcheables types ...
            $expr = $this->wb->getExpresiveWhere($column, (string)$value);
            if ("" === $expr) {
                $expr = $queryBuilder->expr()->eq($column, $this->wb->createParameter($value));
            }
        }
        $queryBuilder->andWhere($expr);
    }

    /**
     * @param string|string[] $value
     */
    private function addConditionEq(QueryBuilder $queryBuilder, string $column, $value) : void
    {
        $param = $this->wb->createParameter($value);
        if (is_iterable($value)) {
            $expr = $queryBuilder->expr()->in($column, $param);
        } else {
            $expr = $queryBuilder->expr()->eq($column, $param);
        }
        $queryBuilder->andWhere($expr);
    }

    /**
     * @param array<mixed> $values
     */
    private function addConditionMember(QueryBuilder $queryBuilder, string $column, $values) : void
    {
        $orx = $queryBuilder->expr()->orX();
        foreach ($values as $v) {
            $param = $this->wb->createParameter($v);
            $expr = $queryBuilder->expr()->isMemberOf($param, $column);
            $orx->add($expr);
        }
        $queryBuilder->andWhere($orx);
    }

    private function sanitizeColumnName(QueryBuilder $queryBuilder, string $column):string
    {
        if (preg_match('@[a-zA-Z]+\.[a-zA-Z].*@', $column)) {
            return $column;
        }

        return $queryBuilder->getRootAlias() . '.' . $column;
    }
}
// vim:sw=4 ts=4 sts=4 et
