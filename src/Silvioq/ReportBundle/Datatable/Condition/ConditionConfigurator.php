<?php

namespace Silvioq\ReportBundle\Datatable\Condition;

use Doctrine\Common\Annotations\Reader;
use Silvioq\ReportBundle\Annotation\Datatable\ConditionDefinition;
use Silvioq\ReportBundle\Annotation\Datatable\ConditionDataMethod;
use Silvioq\ReportBundle\Attribute\Datatable\ConditionDataMethod as AttributeConditionDataMethod;
use Silvioq\ReportBundle\Attribute\Datatable\ConditionDefinition as AttributeConditionDefinition;

/**
 * Datatable Configurator for Conditions
 */
class ConditionConfigurator
{
    /**
     * @var array<array{columnName: string, type: ?string, callback: null|callable|\ReflectionMethod}>
     */
    private $conditions;

    private ?string $method = null;

    const VALID_TYPES = ["like", "in", "gte", "gt", "lte", "lt", "eq"];

    /**
     * Adds one configuration
     *
     * @param string $filterName Filter name
     * @param string $columnName Column name (ex. a.column).
     * @param string|callable|\ReflectionMethod $type Type of filter.
     *
     * @return self
     */
    public function add(string $filterName, string $columnName, $type): self
    {
        if (\is_string($type)) {
            if (!in_array($type, self::VALID_TYPES)) {
                throw new \InvalidArgumentException('Type is invalid.');
            }

            $this->conditions[$filterName] = [
                'columnName' => $columnName,
                'type' => $type,
                'callback' => null
            ];
        } else {
            $this->conditions[$filterName] = [
                'columnName' => $columnName,
                'type' => null,
                'callback' => $type
            ];
        }

        return $this;
    }

    public function setMethod(string $method):self
    {
        $this->method = $method;

        return $this;
    }

    public function getMethod():?string
    {
        return $this->method;
    }

    /**
     * Returns a configuration
     * 
     * @return array{columnName: string, type: ?string, callback: null|callable|\ReflectionMethod}
     */
    public function get(string $filterName):array
    {
        if (isset($this->conditions[$filterName])) {
            return $this->conditions[$filterName];
        }

        throw new \OutOfBoundsException(sprintf("Filter %s is not configured.", $filterName));
    }

    /**
     * Load configuration from class
     *
     * @param class-string $className
     *
     * @return self
     */
    static public function loadFromClass(Reader $reader, string $className)
    {
        new ConditionDefinition; # ¿Required?
        new ConditionDataMethod; # ¿Required?

        $definition = new self;
        $methodDetected = false;

        $class = new \ReflectionClass($className);
        foreach ($reader->getClassAnnotations($class) as $def) {
            if ($def instanceof ConditionDefinition) {
                $definition->add($def->filter, $def->column, $def->type);
            } else if ($def instanceof ConditionDataMethod) {
                $definition->setMethod($def->value);
                $methodDetected = true;
            }
        }

        foreach ($class->getMethods() as $method) {
            foreach ($reader->getMethodAnnotations($method) as $def) {
                if ($def instanceof ConditionDefinition) {
                    $definition->add($def->filter, $def->column, $method);
                }
            }
        }

        // ahora busco atributos
        foreach ($class->getAttributes() as $attr) {
            if ($attr->newInstance() instanceof AttributeConditionDefinition) {
                $definition->add($attr->newInstance()->filter, $attr->newInstance()->column, $attr->newInstance()->type);
            } else if ($attr->newInstance() instanceof AttributeConditionDataMethod) {
                $definition->setMethod($attr->newInstance()->value);
                $methodDetected = true;
            }
        }

        // y los atributos de los métodos
        foreach ($class->getMethods() as $method) {
            foreach ($method->getAttributes() as $attr) {
                if ($attr->newInstance() instanceof AttributeConditionDefinition) {
                    $definition->add($attr->newInstance()->filter, $attr->newInstance()->column, $method);
                }
            }
        }

        if (!$methodDetected && $class->hasMethod('getData')) {
            $definition->setMethod('getData');
        }

        return $definition;
    }
}
// vim:sw=4 ts=4 sts=4 et
