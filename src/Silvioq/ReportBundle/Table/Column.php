<?php declare(strict_types=1);

namespace Silvioq\ReportBundle\Table;

/**
 * @author silvioq
 */
class Column
{
    private string $name;
    
    /**
     * @var string|callable|null
     */
    private $getter;
    private string $label;

    public function __construct(string $name, string $label = null, string|callable $getter = null )
    {
        if (null === $label) {
            $label = static::humanize($name);
        }

        $this->name = $name;
        $this->label = $label;
        $this->getter = $getter;
    }

    public function getName():string
    {
        return $this->name;
    }

    public function getLabel():string
    {
        return $this->label;
    }

    /**
     * @return null|callable|string
     */
    public function getGetter()
    {
        return $this->getter;
    }

    /**
     * @see https://github.com/symfony/symfony/blob/master/src/Symfony/Component/Form/FormRenderer.php#L306
     */
    static public function humanize(string $text):string
    {
        return ucfirst(trim(strtolower(preg_replace(array('/([A-Z])/', '/[_\s]+/'), array('_$1', ' '), $text))));
    }
}
// vim:sw=4 ts=4 sts=4 et
