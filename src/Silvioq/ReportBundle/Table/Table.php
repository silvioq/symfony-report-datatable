<?php declare(strict_types=1);

namespace Silvioq\ReportBundle\Table;

use Silvioq\ReportBundle\Util\Scalarize;
use NumberFormatter;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * @author silvioq
 */
class Table
{
    /** @var array<string, Column> */
    private array $columns;
    
    /** @var class-string */
    private readonly string $entityClass;

    private readonly Scalarize $scalarizer;

    private readonly PropertyAccessorInterface $accessor;

    /**
     * @param class-string $entityClass
     * @param array<string, scalar|callable|NumberFormatter> $scalarizerOption
     */
    public function __construct($entityClass, array $scalarizerOption = array())
    {
        $this->entityClass = $entityClass;
        $this->columns = [];
        $this->scalarizer = new Scalarize($scalarizerOption);

        $this->accessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->getPropertyAccessor();
    }

    /**
     * @return class-string
     */
    public function getEntityClass():string
    {
        return $this->entityClass;
    }

    /**
     * Add field.
     * 
     * @param string|callable|null $getter
     *
     * @return self
     */
    public function add(string $name, string $label = null, $getter = null):self
    {
        if( isset( $this->columns[$name] ) )
            throw new \InvalidArgumentException(\sprintf('Column %s already exists', $name));

        $this->columns[$name] = new Column($name, $label, $getter);
        return $this;
    }

    /**
     * Add expansible
     *
     * @param string $name  Generic name of column
     * @param string|callable|null $getter  Getter of entity collection
     * @param array<mixed>|\Traversable<mixed> $targetCollection  Collection of elements
     * @param string $labelPrefix
     *
     * @return self
     */
    public function addExpansible(string $name, $getter, $targetCollection, string $labelPrefix = '')
    {
        if (null === $getter)
            $getter = 'get' . \ucfirst( $name );

        foreach ($targetCollection as $targetEntity) {
            $subColumnName = $this->scalarizer->scalarize($targetEntity);
            $columnName = $name.'.'.$subColumnName;

            $callback = function($entity) use($targetEntity,$getter){
                if( is_callable( $getter ) ) {
                    $entityList = $getter($entity);
                } else
                    $entityList = $entity->$getter();

                if( !is_array($entityList) && !$entityList instanceof \Traversable )
                    return false;

                foreach( $entityList as $selectedEntity ) {
                    if( $targetEntity === $selectedEntity ) {
                        return true;
                    }
                }
                return false;
            };

            $this->add( $columnName, $labelPrefix . $subColumnName, $callback );
        }
        return $this;
    }

    /**
     * @return self
     *
     * @throws \OutOfBoundsException
     */
    public function removeField(string $name ):self
    {
        if( !isset( $this->columns[$name] ) ) {
            throw new \OutOfBoundsException( sprintf( 'Column %s does not exists', $name ) );
        }

        unset( $this->columns[$name] );

        return $this;
    }

    /**
     * Return initialized status of table definition
     *
     * @return bool
     */
    public function isInitialized():bool
    {
        return count($this->columns) > 0;
    }

    /**
     * @return string[]
     */
    public function getHeader():array
    {
        if( !$this->isInitialized() )
            throw new \LogicException("Generator not initialized");

        return array_map( function(Column $col){
            return $col->getLabel();
        }, array_values($this->columns) );
    }

    /**
     * Return table row with scalar values
     * @return scalar[]
     */
    public function getRow(object $entity):array
    {
        $data = $this->getRawData($entity);
        foreach( $data as &$row ) $row = $this->scalarizer->scalarize($row);
        return $data;
    }

    /**
     * Return table row
     * @return mixed[]
     */
    public function getRawData(object $entity):array
    {
        if( !$this->isInitialized() )
            throw new \LogicException("Generator not initialized");

        if( !($entity instanceof $this->entityClass ) )
            throw new \InvalidArgumentException(sprintf( "Argument 1 must be an instance of %s", $this->entityClass ) );

        return array_map( function(Column $col)use($entity){
                $getter = $col->getGetter();
                if (is_callable($getter)) {
                    return $getter($entity);
                } else if (\is_string($getter)) {
                    if (\method_exists($entity, $getter)) {
                        return $entity->$getter();
                    }
                    return $this->accessor->getValue($entity, $getter);
                } else {
                    return $this->accessor->getValue($entity, $col->getName());
                }
            }, \array_values($this->columns) );
    }
}
// vim:sw=4 ts=4 sts=4 et
