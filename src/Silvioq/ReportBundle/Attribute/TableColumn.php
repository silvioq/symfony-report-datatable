<?php

namespace Silvioq\ReportBundle\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD|Attribute::TARGET_PROPERTY)]
class TableColumn
{
    public ?string $name;
    public ?string $label;
    public ?string$getter;
    public int|float $order = INF;

    /**
     * Flag for expand ManyToMany associations
     */
    public bool $expandMTM;

    /**
     * function name for retrieve all elements for expansion
     */
    public string $expandFinder;

    /**
     * @var int|null
     * 
     * @internal
     * 
     * Internal use only. Do not use it in your code.
     */
    public ?int $key;

    public function __construct(
        string $name = null,
        string $label = null,
        string $getter = null,
        int $order = null,
        bool $expandMTM = false,
        string $expandFinder = 'findAll'
    ) {
        $this->name = $name;
        $this->label = $label;
        $this->getter = $getter;
        $this->order = $order ?? INF;
        $this->expandMTM = $expandMTM;
        $this->expandFinder = $expandFinder;
    }
}
// vim:sw=4 ts=4 sts=4 et
