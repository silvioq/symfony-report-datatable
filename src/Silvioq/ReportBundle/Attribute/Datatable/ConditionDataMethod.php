<?php declare(strict_types=1);

namespace Silvioq\ReportBundle\Attribute\Datatable;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class ConditionDataMethod
{
    public string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }
}