<?php declare(strict_types=1);

namespace Silvioq\ReportBundle\Attribute\Datatable;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class ConditionDefinition
{
    public string $column;
    public ?string $filter;
    public string $type;

    public function __construct(
        string $column,
        ?string $filter = null,
        string $type = "like"
    ) {
        $this->column = $column;
        $this->filter = $filter;
        $this->type = $type;
    }
}