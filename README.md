# Doctrine - Symfony - Datatable Integration

* Simple class for [Datatables](https://datatables.net) integration.

## Version compatibility

| Version | Symfony version | Doctrine |
|---------|-----------------|----------|
| 1.x     | 3.x             | 2.x      |
| 2.x     | 4.x             | 2.x      |
| 3.x     | 5.x             | 2.x      |
| 4.0.x   | 6.x             | ^2.9      |
| 4.3.x   | 6.x             | ^3.2      |

## Symfony 4

```php
# config/bundles.php
...
    Silvioq\ReportBundle\SilvioqReportBundle::class => ['all' => true],
...
```

## Datatable usage

TODO

## Table usage

Table is a simple class for generating large reports (or CSV) from your entities

Simple usage

```php
<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AppBundle\Entity\Entity;

class SimpleController extends Controller
{
    public function downloadAction()
    {
        $table =  $this->get('silvioq.report.table')->build(Entity::class);
                $em = $this->getDoctrine()->getManager();
        $response = new StreamedResponse();
        $iterator = $em->getRepository(Entity::class)
                ->createQueryBuilder('a')
                ->getQuery()->iterate();

        $trans = $this->get('translator');
        $header = array_map(function($header)use($trans){
                return $trans->trans($header);
            }, $table->getHeader() );

        $response->setCallback(function() use($iterator,$table, $header){
            $f = fopen( "php://output", "w" );
            try
            {
                fputcsv( $f, $header );
                foreach( $iterator as $row )
                {
                    $entity = $row[0];
                    fputcsv( $f, $table->getRow($entity) );
                }
            }
            catch(\Exception $e )
            {
                echo $e->getMessage();
                return;
            }
            fclose( $f );
        });
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'entity.csv' );
        $response->headers->set('Content-Type','text/csv');

        return $response;
    }
}
```


[![pipeline status](https://gitlab.com/silvioq/symfony-report-datatable/badges/master/pipeline.svg)](https://gitlab.com/silvioq/symfony-report-datatable/commits/master)
[![coverage report](https://gitlab.com/silvioq/symfony-report-datatable/badges/master/coverage.svg)](https://gitlab.com/silvioq/symfony-report-datatable/commits/master)


## Testeo
```bash
docker run -ti --rm \
  --user $(id -u):$(id -g) -v $(pwd):/app -w /app \
  -v $HOME/.cache/composer:/opt/composer/cache \
  registry.gitlab.com/silvioq/php-base-image:8.1 \
  composer install
docker run -ti --rm \
  --user $(id -u):$(id -g) -v $(pwd):/app -w /app \
  -v $HOME/.cache/composer:/opt/composer/cache \
  registry.gitlab.com/silvioq/php-base-image:8.1 \
  vendor/bin/phpunit
```
